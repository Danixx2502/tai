function hacerPeticionPorId(albumId) {
    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/albums/${albumId}`;

    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById("lista");
            res.innerHTML = ""; // Limpiar la tabla antes de mostrar los nuevos resultados
            const datos = JSON.parse(this.responseText);

            res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>'
                + '<td class="columna2">' + datos.id + '</td>'
                + '<td class="columna3">' + datos.title + '</td> </tr>';

            res.innerHTML += "</tbody>";
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscar").addEventListener("click", function() {
    let albumId = document.getElementById("albumId").value;
    if (albumId !== "") {
        alert("Buscando por ID...");
        hacerPeticionPorId(albumId);
    } else {
        alert("Por favor, ingrese un ID de álbum.");
    }
});

document.getElementById("btnLimpiar").addEventListener("click", function() {
    let res = document.getElementById("lista");
    res.innerHTML = "";
});
